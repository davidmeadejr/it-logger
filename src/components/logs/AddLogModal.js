import React, { useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addLog } from "../../actions/logActions";
import TechSelectOptions from "../techs/TechSelectOptions";
import M from "materialize-css/dist/js/materialize.min.js";

// AddLogModal component allows a user to log an issue with a technician to the system.
const AddLogModal = ({ addLog }) => {
  // The useState hook is used to manage the message, technician and "attention required" checkbox states.
  const [message, setMessage] = useState("");
  const [tech, setTech] = useState("");
  const [attention, setAttention] = useState(false);
  // const techSelectRef = useRef(null); // Uses the useRef hook and assigns it to the variable "techSelectRef"

  // The function onSubmit handles the form submission.
  const onSubmit = () => {
    // Checks if message and tech fields are not empty.
    if (message === "" || tech === "") {
      M.toast({ html: "Please enter a message and select a technician" });
    } else {
      // If not empty, create a new log object with the input fields.
      const newLog = {
        message,
        tech, // The technicians are accessed via the <TechSelectOptions/> component which handles the tech dropdown.
        attention,
        date: new Date(),
      };

      // Pass the newLog object to the addLog action and store it in the db.json database.
      addLog(newLog);

      M.toast({ html: `Log added by ${tech}` });

      // Clear the input fields.
      setMessage("");
      setTech("");
      setAttention(false);
    }
  };

  return (
    <div id="add-log-modal" className="modal" style={modalStyle} data-testid="add-log-modal">
      <div className="modal-content">
        <h4>Enter System Log</h4>
        <div className="input-field">
          <label htmlFor="message">Log Message</label>
          <input
            id="message"
            type="text"
            name="message"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            data-testid="log-message"
          />
        </div>
        <div className="row">
          <div className="input-field" value={tech} onChange={(e) => setTech(e.target.value)}>
            <TechSelectOptions />
          </div>
        </div>
        <div className="input-field">
          <p>
            <label>
              <input
                type="checkbox"
                className="filled-in"
                checked={attention}
                value={attention}
                onChange={(e) => setAttention(!attention)}
                data-testid="attention-checkbox"
              />
              <span>Needs Attention</span>
            </label>
          </p>
        </div>

        <div className="modal-footer">
          <button onClick={onSubmit} className="modal-close waves-effect blue waves-light btn">
            Enter
          </button>
        </div>
      </div>
    </div>
  );
};

// Expected prop types the AddLogModal component expects to receive.
AddLogModal.propTypes = {
  addLog: PropTypes.func.isRequired,
};

// Sets the width and height of the modal.
const modalStyle = {
  width: "75%",
  height: "75%",
};

export default connect(null, { addLog })(AddLogModal);
