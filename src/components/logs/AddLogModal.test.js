import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../../reducers";
import AddLogModal from "./AddLogModal";

const mockAddLog = jest.fn();
const middleware = [thunk];

const mockStore = createStore(
  rootReducer,
  {
    log: {
      logs: [],
    },
    tech: {
      techs: [{ id: 1, firstName: "John", lastName: "Doe" }],
      loading: false,
    },
  },
  applyMiddleware(...middleware)
);

mockStore.dispatch = mockAddLog;

describe("AddLogModal component", () => {
  it("renders correctly", () => {
    const { getByTestId, getByLabelText, getByText } = render(
      <Provider store={mockStore}>
        <AddLogModal />
      </Provider>
    );
    expect(getByTestId("add-log-modal")).toBeTruthy();
    expect(getByLabelText("Log Message")).toBeTruthy();
    expect(getByText("Select Technician")).toBeTruthy();
    expect(getByText("John Doe")).toBeTruthy();
  });
});

console.error = jest.fn();
