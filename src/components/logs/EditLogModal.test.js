import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../store";
import EditLogModal from "./EditLogModal";

describe("EditLogModal", () => {
  const current = { id: 1, message: "Test log message", tech: "John Doe", attention: false, date: new Date() };
  const mockUpdateLog = jest.fn();

  it("renders the EditLogModal component", () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <EditLogModal current={current} updateLog={mockUpdateLog} />
      </Provider>
    );
    expect(getByTestId("edit-log-modal")).toBeTruthy();
  });

  it("updates the log message", () => {
    const { getByLabelText } = render(
      <Provider store={store}>
        <EditLogModal current={current} updateLog={mockUpdateLog} />
      </Provider>
    );
    const logMessageInput = getByLabelText("Log Message");
    fireEvent.change(logMessageInput, { target: { value: "Updated log message" } });
    expect(logMessageInput.value).toBe("Updated log message");
  });
});
