import React from "react";
import Moment from "react-moment";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteLog, setCurrent } from "../../actions/logActions";
import M from "materialize-css/dist/js/materialize.min.js";

// LogItem displays an individual log item in the UI.
const LogItem = ({ log, deleteLog, setCurrent }) => {
  // When the delete button is clicked, the onDelete function called which calls the deleteLog action to delete the log from the database.
  const onDelete = () => {
    deleteLog(log.id);
    M.toast({ html: "Log Deleted" });
  };

  return (
    <li className="collection-item">
      <div>
        <a
          href="#edit-log-modal"
          className={`modal-trigger ${log.attention ? "red-text" : "blue-text"}`}
          onClick={() => setCurrent(log)}
        >
          {log.message}
        </a>
        <br />
        <span className="grey-text">
          <span className="black-text">ID #{log.id}</span> last updated by{" "}
          <span className="black-text">{log.tech}</span> on <Moment format="MMM Do YYYY, h:mm:ssa">{log.date}</Moment>
        </span>
        <a href="#!" onClick={onDelete} className="secondary-content" data-testid="delete-log-button">
          <i className="material-icons grey-text">delete</i>
        </a>
      </div>
    </li>
  );
};

// Expected prop types the LogItem component expects to receive.
LogItem.propTypes = {
  log: PropTypes.object.isRequired,
  deleteLog: PropTypes.func.isRequired,
  setCurrent: PropTypes.func.isRequired,
};

export default connect(null, { deleteLog, setCurrent })(LogItem);
