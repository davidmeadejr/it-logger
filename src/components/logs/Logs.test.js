import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../../reducers";
import Logs from "./Logs";

const mockGetLogs = jest.fn();

const middleware = [thunk];

const mockStore = createStore(
  rootReducer,
  {
    log: {
      logs: [
        { id: 1, message: "Test log 1" },
        { id: 2, message: "Test log 2" },
      ],
      loading: false,
    },
  },
  applyMiddleware(...middleware)
);

mockStore.dispatch = mockGetLogs;

describe("Logs component", () => {
  it("renders correctly", () => {
    const { getByTestId, getByText } = render(
      <Provider store={mockStore}>
        <Logs />
      </Provider>
    );
    expect(getByTestId("logs")).toBeTruthy();
    expect(getByText("System Logs")).toBeTruthy();
    expect(getByText("Test log 1")).toBeTruthy();
    expect(getByText("Test log 2")).toBeTruthy();
  });

  it("calls the getLogs action when the component mounts", () => {
    render(
      <Provider store={mockStore}>
        <Logs />
      </Provider>
    );
    expect(mockGetLogs).toHaveBeenCalled();
  });

  it("displays a message when there are no logs to show", () => {
    const mockEmptyStore = createStore(
      rootReducer,
      {
        log: {
          logs: [],
          loading: false,
        },
      },
      applyMiddleware(...middleware)
    );

    const { getByText } = render(
      <Provider store={mockEmptyStore}>
        <Logs />
      </Provider>
    );
    expect(getByText("No logs to show...")).toBeTruthy();
  });
});
