import React, { useState, useEffect } from "react";
import TechSelectOptions from "../techs/TechSelectOptions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateLog } from "../../actions/logActions";
import M from "materialize-css/dist/js/materialize.min.js";

// The EditLogModal component enables users to edit a log already stored in the database.
const EditLogModal = ({ current, updateLog }) => {
  // The hook useState manges the state of the message input, attention checkbox and technician.
  const [message, setMessage] = useState("");
  const [attention, setAttention] = useState(false);
  const [tech, setTech] = useState("");

  // Sets the message input to the current message and attention checkbox to the current status.
  useEffect(() => {
    if (current) {
      setMessage(current.message);
      setAttention(current.attention);
    }
    //eslint-disable-next-line
  }, [current]);

  // The onSubmit function handles the functionality of the form submission.
  const onSubmit = () => {
    if (message === "" || tech === "") {
      M.toast({ html: "Please enter a message and tech" });
    } else {
      const updatedLog = {
        id: current.id,
        message,
        tech,
        attention,
        date: new Date(),
      };

      updateLog(updatedLog); // Saves the updated log via the updateLog action.
      M.toast({ html: `Log updated by ${tech}` });

      // Clear message and tech input fields along with the attention checkbox.
      setMessage("");
      setTech("");
      setAttention(false);
    }
  };

  return (
    <div id="edit-log-modal" className="modal" style={modalStyle} data-testid="edit-log-modal">
      <div className="modal-content">
        <h4>Edit System Log</h4>
        <div className="row">
          <div className="input-field">
            <label htmlFor="message">Log Message</label>
            <input
              type="text"
              id="message"
              name="message"
              placeholder="Log Message"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="input-field" value={tech} onChange={(e) => setTech(e.target.value)}>
            <TechSelectOptions />
          </div>
        </div>
        <div className="row">
          <div className="input-field">
            <p>
              <label>
                <input
                  type="checkbox"
                  className="filled-in"
                  checked={attention}
                  value={attention}
                  onChange={(e) => setAttention(!attention)}
                />
                <span>Needs Attention</span>
              </label>
            </p>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <a href="#!" onClick={onSubmit} className="modal-close waves-effect blue waves-light btn">
          Enter
        </a>
      </div>
    </div>
  );
};

// Sets the width and height of the modal.
const modalStyle = {
  width: "75%",
  height: "75%",
};

// Expected prop types the EditLogModal component expects to receive.
EditLogModal.propTypes = {
  current: PropTypes.object,
  updateLog: PropTypes.func.isRequired,
};

// Maps ths state to the component props.
const mapStateToProps = (state) => ({
  current: state.log.current,
});

export default connect(mapStateToProps, { updateLog })(EditLogModal);
