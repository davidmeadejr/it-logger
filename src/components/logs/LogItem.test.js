import React from "react";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import rootReducer from "../../reducers";
import { Provider } from "react-redux";
import { render, fireEvent } from "@testing-library/react";
import LogItem from "./LogItem";

const initialState = {};
const middleware = [thunk];
const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));

describe("LogItem component", () => {
  const log = {
    id: 1,
    message: "Test log message",
    attention: false,
    tech: "Test tech",
    date: new Date(),
  };

  it("renders log message", () => {
    const { getByText } = render(
      <Provider store={store}>
        <LogItem log={log} />
      </Provider>
    );
    expect(getByText("Test log message")).toBeTruthy();
  });

  it("renders log ID", () => {
    const { getByText } = render(
      <Provider store={store}>
        <LogItem log={log} />
      </Provider>
    );
    expect(getByText("ID #1")).toBeTruthy();
  });

  it("renders log tech", () => {
    const { getByText } = render(
      <Provider store={store}>
        <LogItem log={log} />
      </Provider>
    );
    expect(getByText("Test tech")).toBeTruthy();
  });

  it("calls deleteLog when delete button is clicked", () => {
    const mockDeleteLog = jest.fn();
    const { getByTestId } = render(
      <Provider store={store}>
        <LogItem log={log} deleteLog={mockDeleteLog} />
      </Provider>
    );
    fireEvent.click(getByTestId("delete-log-button"));
    expect(mockDeleteLog).toBeTruthy();
  });
});
