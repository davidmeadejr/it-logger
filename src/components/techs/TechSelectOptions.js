import React, { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getTechs } from "../../actions/techActions";

// TechSelectOptions component creates a dropdown of all the technicians.
const TechSelectOptions = ({ tech: { techs, loading }, getTechs }) => {
  // The useEffect hook is used to fetch the technicians when the component mounts.
  useEffect(() => {
    getTechs();
  }, [getTechs]);

  // Only render the component if loading is false and techs is not null.
  return !loading && techs !== null ? (
    <select className="browser-default" defaultValue="" data-testid="tech-select-options">
      <option disabled value="">
        Select Technician
      </option>
      {/*  map through the techs array and renders an option element for each technician. */}
      {techs.map((t) => (
        <option key={t.id} value={`${t.firstName} ${t.lastName}`}>
          {t.firstName} {t.lastName}
        </option>
      ))}
    </select>
  ) : null;
};

// Expected prop types the TechSelectOptions component expects to receive.
TechSelectOptions.propTypes = {
  tech: PropTypes.object.isRequired,
  getTechs: PropTypes.func.isRequired,
};

// Maps ths state to the component props.
const mapStateToProps = (state) => ({
  tech: state.tech,
});

export default connect(mapStateToProps, { getTechs })(TechSelectOptions);
