import React, { useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addTech } from "../../actions/techActions";
import M from "materialize-css/dist/js/materialize.min.js";

// The AddTechModal component allows a user to add new technicians to the system.
const AddTechModal = ({ addTech }) => {
  // The hook useState is use to manage the first and last name of a new technician.
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  // The function onSubmit handles the form submission of adding a new technician to the system.
  const onSubmit = () => {
    if (firstName === "" || lastName === "") {
      M.toast({ html: "Please enter the first and last name" });
    } else {
      addTech({ firstName, lastName }); // Calls the addTech action which adds a new technician to the db.
      M.toast({ html: `${firstName} ${lastName} was added as a tech` });
      // Clears the form fields after save.
      setFirstName("");
      setLastName("");
    }
  };

  return (
    <div id="add-tech-modal" className="modal">
      <form onSubmit={onSubmit} className="modal-content" data-testid="add-tech-modal">
        <h4>New Technician</h4>
        <div className="input-field">
          <label htmlFor="lastName" className="active">
            First Name
          </label>
          <input
            type="text"
            id="firstName"
            name="firstName"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </div>
        <div className="input-field">
          <label htmlFor="lastName" className="active">
            Last Name
          </label>
          <input
            type="text"
            id="lastName"
            name="lastName"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
        </div>
        <div className="modal-footer">
          <button type="submit" className="modal-close waves-effect blue waves-light btn">
            Enter
          </button>
        </div>
      </form>
    </div>
  );
};

// Prototypes for the component.
AddTechModal.propTypes = {
  addTech: PropTypes.func.isRequired,
};

export default connect(null, { addTech })(AddTechModal);
