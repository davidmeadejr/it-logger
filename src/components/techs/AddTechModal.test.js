import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../../reducers";
import AddTechModal from "./AddTechModal";

const mockAddTech = jest.fn();

const middleware = [thunk];

const mockStore = createStore(
  rootReducer,
  {
    tech: {
      techs: [],
      loading: false,
    },
  },
  applyMiddleware(...middleware)
);

mockStore.dispatch = mockAddTech;

describe("AddTechModal component", () => {
  it("renders correctly", () => {
    const { getByTestId, getByLabelText, getByText } = render(
      <Provider store={mockStore}>
        <AddTechModal />
      </Provider>
    );
    expect(getByTestId("add-tech-modal")).toBeTruthy();
    expect(getByLabelText("First Name")).toBeTruthy();
    expect(getByLabelText("Last Name")).toBeTruthy();
    expect(getByText("Enter")).toBeTruthy();
  });
});
