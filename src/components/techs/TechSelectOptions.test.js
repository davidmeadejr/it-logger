import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../../reducers";
import TechSelectOptions from "./TechSelectOptions";

const mockAddTech = jest.fn();

const middleware = [thunk];

const mockStore = createStore(
  rootReducer,
  {
    tech: {
      techs: [],
      loading: false,
    },
  },
  applyMiddleware(...middleware)
);

mockStore.dispatch = mockAddTech;

describe("TechSelectOptions component", () => {
  it("renders correctly", () => {
    const { getByTestId, getByText } = render(
      <Provider store={mockStore}>
        <TechSelectOptions />
      </Provider>
    );
    expect(getByTestId("tech-select-options")).toBeTruthy();
    expect(getByText("Select Technician")).toBeTruthy();
  });
});
